<?php

namespace App\Controller;

// use App\Article\ArticlesYamlProvider;
use App\Article\ArticleApiProvider;
use App\Article\ArticlesYamlProvider;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     * @param ArticlesYamlProvider
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ArticlesYamlProvider $articles, ArticleApiProvider $articleApiProvider)
    {
        // $articles = new ArticlesYamlProvider();
        // dump($articles->getArticles());
        $articleApi = $articleApiProvider->getArticles();
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'yamlArticles' => $articles->getArticles(),
            'apiArticles' => $articleApi['articles']
        ]);
    }
}

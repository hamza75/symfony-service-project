<?php

namespace App\Article;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

class ArticlesYamlProvider implements ArticleProviderInterface
{
    /**
     * Retour un parse de file articles.yaml
     *
     * @return void
     */
    public function getArticles(): iterable
    {

        try {
            $value = Yaml::parseFile(__DIR__.'/Articles.yaml');
        } catch (ParseException $exception) {
            printf('Unable to parse the YAML string: %s', $exception->getMessage());
        }
        return $value;
    }
    
}
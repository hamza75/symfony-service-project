<?php

namespace App\Article;

interface ArticleProviderInterface
{
    /**
     * retourner les articles
     *
     * @return iterable
     */
    public function getArticles(): iterable;
    
}
<?php

namespace App\Article;

use Symfony\Component\HttpClient\HttpClient;
// use Symfony\Component\HttpClient\NativeHttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ArticleApiProvider implements ArticleProviderInterface
{
    private $httpClient;
    private $newsApiKey;
    public function __construct(HttpClientInterface $httpClient, string $newsApiKey)
    {
        $this->httpClient = $httpClient;
        $this->newsApiKey = $newsApiKey;
    }
    
    /**
     * @ iterable
     */
    public function getArticles(): iterable
    {
        /**
         * 
         */
        
        // TODO deplacer la clee api ds file config yaml
        $response = $this->httpClient->request('GET', 'https://newsapi.org/v2/everything?q=bitcoin&from=2019-10-23&sortBy=publishedAt&apiKey=' . $this->newsApiKey);

        return $response->toArray();

    }
    
}